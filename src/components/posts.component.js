import { html, LitElement } from "lit";
import { AllPostsUseCase } from "../usecases/all-posts.usecase";
import { OddPostsUseCase } from "../usecases/odd-posts.usecase";
import "./../ui/posts.ui";
import { snapshot } from "valtio/vanilla";
import { state } from "../states/state";

export class PostsComponent extends LitElement {
  static get properties() {
    return {
      posts: {
        type: Array,
        state: true,
      },
      count: {
        type: Number,
        state: true,
      },
      text: {
        type: String,
        state: true,
      },
    };
  }

  async connectedCallback() {
    super.connectedCallback();
    const snap = snapshot(state);
    this.count = snap.count;
    this.text = snap.text;
    const allPostsUseCase = new AllPostsUseCase();
    this.posts = await allPostsUseCase.execute();
  }

  async allOdds() {
    const oddPostsUseCase = new OddPostsUseCase();
    this.posts = await oddPostsUseCase.execute();
  }

  render() {
    return html`
      <p>${this.count}--${this.text}</p>
      <button @click="${this.allOdds}" id="oddAction">Odd</button>
      <posts-ui .posts="${this.posts}"></posts-ui>
    `;
  }

  createRenderRoot() {
    return this;
  }
}

customElements.define("genk-posts", PostsComponent);
