import { subscribe } from "valtio/vanilla";
import { state } from "../states/state";

export class HomePage extends HTMLElement {

  unsubscribe;

  constructor() {
    super();
  }

  connectedCallback() {
    this.render();
    this.unsubscribe = subscribe(state, () => {
      this.render();
    });
  }

  disconnectedCallback() {
    this.unsubscribe();
  }

  render() {
    this.innerHTML = `
     <h1>Vanilla</h1>
     <p>${state.count} --- ${state.text}</p>
     <button id="add">Add</button>
     <app-element hello="Hola Pablo"><p>¿Qué se va a mostrar?</p>
    </app-element>
    
    `;
    const button = document.querySelector("#add");
    button.addEventListener("click", () => {
      ++state.count;
      state.text = `Cambio ${state.count}`;
      console.log(state);
    });
  }
}
customElements.define("home-page-genk", HomePage);
